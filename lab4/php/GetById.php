<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {  

    $id = $_GET['id'];
    
    $conn = mysqli_connect('localhost','Daryna', '123456781', 'pvi_db');
    
    if(!$conn){
        http_response_code(400);
        echo "Connection error!!!". mysqli_connect_error();
        exit;
    }

    $sql = "SELECT* FROM students WHERE id = '$id'";

    $res = mysqli_query($conn, $sql);

    $students_fetch = mysqli_fetch_assoc($res);

    if($students_fetch !== null){
        http_response_code(200);  
        echo json_encode($students_fetch, JSON_PRETTY_PRINT); 
    }
    else{
        http_response_code(405); 
        echo json_encode(array("error" => "The student with identifier $id was not found."), JSON_PRETTY_PRINT);
    }
    
    mysqli_close($conn);

} else {  
    $response = [  
        'success' => false,  
        'message' => 'The query method is not supported.'  
    ];  
    http_response_code(405);  
    echo json_encode($response, JSON_PRETTY_PRINT); 
}  

?>