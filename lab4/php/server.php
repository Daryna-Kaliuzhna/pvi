<?php  
$responses=[];
header("Access-Control-Allow-Origin: *");   
header("Access-Control-Allow-Methods: *");    
header("Access-Control-Allow-Headers: Content-Type"); 

function isValidData($data){

    $isValid = true;
    
    if(!isDataEntered($data)) $isValid = false;
    if(!isValidName($data)) $isValid = false;
    if(!isValidSurname($data)) $isValid = false;
    if(!isValidGender($data)) $isValid = false;
    if(!isValidGroup($data)) $isValid = false;
    if(!isValidNameFormat($data)) $isValid = false;
    if(!isValidDateFormat($data)) $isValid = false;

    return $isValid;
}
function isDataEntered($data){
    global $responses;
    if (!isset($data['firstname'], $data['lastname'], $data['gender'], $data['birthday'], $data['group_name'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Incomplete data received.'  
        ];  
       return false; 
    }  
    return true;
}

function isValidName($data){
    global $responses;  
    if (empty($data['firstname'])) {
        $responses[] = [  
            'success' => false,  
            'message' => 'FirstName error.' 
        ];  
        return false;
    }  
    return true;
}
function isValidSurname($data){
    global $responses;  
    if (empty($data['lastname'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Lastname error.'
        ];  
        return false;
    }  
    return true;
}
function isValidGender($data){
    global $responses;
    if (!isset($data['gender'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Gender error.'  
        ];  
        return false;
    }  
    return true;
}
function isValidGroup($data){
    global $responses;
    if (!isset($data['group_name'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Group error.'  
        ];  
        return false;
    }  
    return true;
}
function isValidNameFormat($data){
    global $responses;
    $firstname = $data['firstname'];  
    $lastname = $data['lastname'];  
    if (!preg_match('/^[A-ZА-ЯЁ][a-zа-яё]+$/u', $firstname) || !preg_match('/^[A-ZА-ЯЁ][a-zа-яё]+$/u', $lastname)) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'First name and last name must start with a capital Latin or Cyrillic letter and contain only Latin or Cyrillic letters.'  
        ];  
        return false;  
    }  
    return true;
}
function isValidDateFormat($data){
    global $responses;
    $birthday = $data['birthday'];  

    $minYear = 1990;  
    $maxYear = 2006;  
    if (strtotime($birthday) < strtotime("$minYear-01-01") || strtotime($birthday) > strtotime("$maxYear-12-31")) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Birthday must be between 1990 and 2006.'  
        ];  
        return false;  
    } 
    return true;
}
    
if ($_SERVER['REQUEST_METHOD'] === 'POST') {  
 
    $data = json_decode(file_get_contents('php://input'), true); 
 
    if(isValidData($data)){
       
        $conn = mysqli_connect('localhost','Daryna', '123456781', 'pvi_db');

        if(!$conn){
            http_response_code(400);
            echo "Connection error!!!". mysqli_connect_error();
            exit;
        }

        $name = $data['firstname']; 
        $lastname = $data['lastname']; 
        $group_name = $data['group_name']; 
        $gender = $data['gender']; 
        $status = false; 
        $birthday = $data['birthday']; 
         
      
        $sql = "INSERT INTO Students VALUES ('', '$name', '$lastname', '$group_name', '$gender', '$birthday', '$status')";

        $res = mysqli_query($conn, $sql);

        if($res !== null){
            http_response_code(200);  
            echo json_encode(array("success" => "Adding successful."), JSON_PRETTY_PRINT);
        }
        else{
            http_response_code(400); 
            echo json_encode(array("error" => "Adding error."), JSON_PRETTY_PRINT);
        }

        mysqli_close($conn);
        
    }else{
        http_response_code(400);
        echo json_encode($responses, JSON_PRETTY_PRINT);
    }

} else {  
    $responses[] = [  
        'success' => false,  
        'message' => 'The query method is not supported.'  
    ];  
    http_response_code(405);  
}  
?>