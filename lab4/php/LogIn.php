<?php  
$responses=[];
header("Access-Control-Allow-Origin: *");   
header("Access-Control-Allow-Methods: *");    
header("Access-Control-Allow-Headers: Content-Type"); 

function isValidUserData($data){

    $isValid = true;
    
    if(!isValidLogin($data)) $isValid = false;
    if(!isValidPassword($data)) $isValid = false;

    return $isValid;

}
function isDataEntered($data){
    global $responses;
    if (!isset($data['login'], $data['password'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Incomplete data received.'  
        ];  
       return false; 
    }  
    return true;
}

function isValidLogin($data){
    global $responses;

    if(!$data['login']){
        $responses[] = [  
            'success' => false,  
            'message' => 'Invalid login format'  
        ];
        return false;
    }

    $emailRegex = '/^[^\s@]+@[^\s@]+\.[^\s@]+$/';
    if (!preg_match($emailRegex, $data['login'])) {
        $responses[] = [  
            'success' => false,  
            'message' => 'Login must be in email format.'  
        ];
        return false;
    }

    return true;
}

function isValidPassword($data){
    if(!$data['password']){
        $responses[] = [  
            'success' => false,  
            'message' => 'Invalid password format'  
        ];
        return false;
    }
    $passwordRegex = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/';
    if (!preg_match($passwordRegex, $data['password'])){
        $responses[] = [  
            'success' => false,  
            'message' => 'The password must contain at least 8 characters, a lowercase letter, an uppercase letter and a number'  
        ];
        return false;
    }
    
    return true;
}


    
if ($_SERVER['REQUEST_METHOD'] === 'POST') {  
 
    $data = json_decode(file_get_contents('php://input'), true); 
 
    if(isValidUserData($data)){
       
        $conn = mysqli_connect('localhost','Daryna', '123456781', 'pvi_db');

        if(!$conn){
            http_response_code(400);
            echo "Connection error!!!". mysqli_connect_error();
            exit;
        }

        $login = $data['login']; 
        $password = $data['password']; 

        $sql = "SELECT* FROM  users WHERE login = '$login' AND password = '$password'";

        $res = mysqli_query($conn, $sql);

        $user_fetch = mysqli_fetch_assoc($res);

        if($user_fetch !== null){
            http_response_code(200);  
            echo json_encode(array("success" => true /*"User authenticated successfully"*/), JSON_PRETTY_PRINT);
            exit;
        }
        else{
            http_response_code(400); 
            echo json_encode(array("error" => "Invalid login or password"), JSON_PRETTY_PRINT);
            exit;
        }

        if(!$responses){
            echo json_encode($responses, JSON_PRETTY_PRINT);
        }
    }else{
        http_response_code(400);
        echo json_encode($responses, JSON_PRETTY_PRINT);
    }
    mysqli_close($conn);

} else {  
    $responses[] = [  
        'success' => false,  
        'message' => 'The query method is not supported.'  
    ];  
    http_response_code(405);  
}  
?>