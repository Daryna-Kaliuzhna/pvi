function toggleForm() {
    var form = document.getElementById('student-form');
    form.classList.toggle('hidden');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block';
}

$(document).ready(function() {
    getStudents();
    $('#btn-add').on('click', function() {
        $('#form-header h2').text('');
        $('#form-header h2').text('Add Student');
        toggleForm();
    });
    
    $('#cancel-btn').on('click', function() {
        closeForm();
    });

    $('#cancel-btn-in').on('click', function() {
        closeFormIn();
    });

    $('#sign-in').on('click', function() {
        toggleFormIN();
    });
});


//Кнопка форми save
function save() {
    var header = $('#form-header h2').text();
    if (header.includes('Add Student')) {
        addStudent();
    } else {
        editStudent();
    }
}


//Зміна форми для редагування
function editOnClick(button) {
    
    var currentRowToEdit = $(button).closest('tr');
    var id = $(currentRowToEdit).find('td:nth-child(1)').text();
    getStudentById(id);

    $('#form-header h2').text('Edit Student');
    toggleForm();
}

//Додавання студента
function addStudent() {

    var group = $('#Group').val();
    var firstname = $('#fname').val();
    var lastname = $('#lname').val();
    var gender = $('#Gender').val();
    var birthday = $('#Bday').val();

    /* if (!isValidStudent(group, firstname, lastname, gender, birthday)) {
        return;
    } */
 
    var studentData = {
        group_name: group,
        firstname: firstname,
        lastname: lastname,
        gender: gender,
        birthday: birthday
    };

    var jsonData = JSON.stringify(studentData);

    $.ajax({
        url: 'http://localhost/lab4/php/server.php',
        method: 'POST',
        data: jsonData,
        success: function(response) {
            
            if (response) {
                console.log('Data saved successfully:', response);
              
                $('#Group').val('');
                $('#fname').val('');
                $('#lname').val('');
                $('#Gender').val('');
                $('#Bday').val('');

                 closeForm();
                 getStudents();

            } else {
                console.error('Error saving data:', response.message);
                alert('Error saving data: ' + response.message);
            }
        },
        error: function(xhr, status, error) {
            const responses = JSON.parse(xhr.responseText); 
            let message = ''; 
    
            responses.forEach(function(item) { 
                message += item.message + '\n';  
            }); 
         
            alert(message); 
           
        }
    });
}


//Закриття форми
function closeForm(){
    $('#Group, #fname, #lname, #Gender, #Bday').val('');

    $('#group-error').text('');
    $('#Group').css("border-color", '');
    $('#name-error').text('');
    $('#fname').css("border-color", '');
    $('#lastname-error').text('');
    $('#lname').css("border-color", '');
    $('#gender-error').text('');
    $('#Gender').css("border-color", '');
    $('#birthday-error').text('');
    $('#Bday').css("border-color", '');

    $('#student-form').toggleClass('hidden'); 
    $('#overlay').hide();
}

//Редагування студента

function editStudent(){
    var id = $('#studentId').val();
    var group = $('#Group').val();
    var firstname = $('#fname').val();
    var lastname = $('#lname').val();
    var gender = $('#Gender').val();
    var birthday = $('#Bday').val();

    /* if (!isValidStudent(group, firstname, lastname, gender, birthday)) {
        return;
    } */

    var studentData = {
        id:id,
        group_name: group,
        firstname: firstname,
        lastname: lastname,
        gender: gender,
        birthday: birthday
    };

    updateStudent(studentData);

    //closeForm();
}

//Валідація даних
function isValidStudent(group, firstname, lastname, gender, birthday){
    var isValid = true;
    if(!isValidGroup(group)) isValid = false;
    if(!isValidName(firstname)) isValid = false;
    if(!isValidSurname(lastname)) isValid =false;
    if(!isValidGender(gender)) isValid = false;
    if(!isValidBday(birthday)) isValid = false;

    return isValid;
}
function isValidGroup(group){
    if(!group){
        $('#group-error').css('display', 'block');
        $('#group-error').text('Group must be selected!');
        $('#Group').css("border-color", 'red');
        return false;
    }
    else{
        $('#group-error').text('');
        $('#Group').css("border-color", '');
        return true;
    }
}
function isValidName(firstname){
    if(!firstname){
        $('#name-error').css('display', 'block');
        $('#name-error').text('First name field cannot be empty!');
        $('#fname').css("border-color", 'red');
        return false;
    }
    var nameRegex = /^[a-zA-Zа-яА-Я]+$/;
    if (!nameRegex.test(firstname)) {

        $('#name-error').css('display', 'block');
        $('#name-error').text('First name should contain only letters!');
        $('#fname').css("border-color", 'red');
        return false;
    }
    else{
        $('#name-error').text('');
        $('#fname').css("border-color", '');
        return true;
    }
}
function isValidSurname(lastname){
    if(!lastname){
        $('#lastname-error').css('display', 'block');
        $('#lastname-error').text('Last name field cannot be empty!');
        $('#lname').css("border-color", 'red');
        return false;
    }
    var nameRegex = /^[a-zA-Zа-яА-Я]+$/;
    if (!nameRegex.test(lastname)) {

        $('#lastname-error').css('display', 'block');
        $('#lastname-error').text('Last name should contain only letters!');
        $('#lname').css("border-color", 'red');
        return false;
    }
    else{
        $('#lastname-error').text('');
        $('#lname').css("border-color", '');
        return true;
    }
}
function isValidGender(gender){
    if(!gender){
        $('#gender-error').css('display', 'block');
        $('#gender-error').text('Gender must be selected!');
        $('#Gender').css("border-color", 'red');
        return false;
    }
    else{
        $('#gender-error').text('');
        $('#Gender').css("border-color", '');
        return true;
    }
}
function isValidBday(birthday){
    if(!birthday){
        $('#birthday-error').css('display', 'block');
        $('#birthday-error').text('Birthday must be selected!');
        $('#Bday').css("border-color", 'red');
        return false;
    }

    var minDate = new Date('1990-01-01');
    var maxDate = new Date('2006-12-31');
    var inputDate = new Date(birthday);
    if (inputDate < minDate || inputDate > maxDate) {
        $('#birthday-error').css('display', 'block');
        $('#birthday-error').text('Birthday should be between 1990 and 2006');
        $('#Bday').css("border-color", 'red');
        return false;
    }
    else{
        $('#birthday-error').text('');
        $('#Bday').css("border-color", '');
        return true;
    }

}


/*function fieldsValidator(group, firstname, lastname, gender, birthday){
    var errorcolor = 'red';

    if (group === '' || firstname === '' || lastname === '' || gender === '' || birthday === '') {
       
        alert('Please fill in all fields');
        return false;
    } 

    var nameRegex = /^[a-zA-Zа-яА-Я]+$/;
    if (!nameRegex.test(firstname) || !nameRegex.test(lastname)) {
        alert('First name and last name should contain only letters');
        return false;
    }

    var minDate = new Date('1990-01-01');
    var maxDate = new Date('2006-12-31');
    var inputDate = new Date(birthday);
    if (inputDate < minDate || inputDate > maxDate) {
        alert('Birthday should be between 1990 and 2006');
        return false;
    }

    return true;
} */


//Видалення студента

let studentIdToDelete;

function warningForm(button){
    var form = document.getElementById('warning-form');
    form.classList.toggle('delete-form');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block'; 

    let currentRowToDelete = button.closest('tr');
   
    var studentNameElement = currentRowToDelete.querySelector('td:nth-child(4)');
    studentIdToDelete = currentRowToDelete.querySelector('td:nth-child(1)').innerText;

    var studentName = studentNameElement.innerText;
  
    document.getElementById("warning-message").innerText = "Are you sure you want to delete user " + studentName + "?";
}
function deleteStudent() {
    deleteStudentById(studentIdToDelete);
    cancelDeleting(); 
}
function cancelDeleting(){
    var form = document.getElementById('warning-form');
    form.classList.toggle('delete-form');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'none'; 
}

function getStudents() {
    
    $('.student-table tbody').empty(); 
    $.ajax({
        url: 'http://localhost/lab4/php/Get.php',
        method: 'GET',
        success: function(response) {
            var res = JSON.parse(response);
            if (response) {
                console.log('Data saved successfully:', response);
                res.forEach(function(student) {
                    addTableRow(student);
                });
            } else {
                console.error('Error saving data:', response.message);
                alert('Error saving data: ' + response.message);
            }
        },
        error: function(xhr, status, error) {
            const responses = JSON.parse(xhr.responseText);
            let message = '';
            responses.forEach(function(item) {
                message += item.message + '\n';
            });
            alert(message);
        }
    });
}

function addTableRow(student) {
    
    var newRow = `
        <tr>
            <td id="id-table">${student.id}</td>
            <td><input type="checkbox"></td>
            <td>${student.group_name}</td>
            <td>${student.firstname} ${student.lastname}</td>
            <td>${student.gender}</td>
            <td>${student.birthday}</td>
            <td class="circle-cell">
                <div style="background-color:${student.status == true ? 'green' : 'grey'}" class="circle"></div>
            </td>
            <td>
                <button id="btn-edit" onclick="editOnClick(this)" class="btn-edit"><img id="img-edit" src="./images/pen.png" alt="edit student"></button> 
                <button id="btn-delete" onclick="warningForm(this)"><img id="img-delete" src="./images/delete.png" alt="delete student"></button>
            </td>
        </tr>
    `;

    // Додаємо новий рядок до таблиці
    $('.student-table tbody').append(newRow);
}


function getStudentById(id) {

    $.ajax({
        url: `http://localhost/lab4/php/GetById.php?id=${id}`,
        method: 'GET',
        success: function(response) {
            var res = JSON.parse(response);
            if (response) {
                console.log('Data saved successfully:', response);

                $('#studentId').val(res.id);
                $('#Group').val(res.group_name);
                $('#fname').val(res.firstname);
                $('#lname').val(res.lastname);
                $('#Gender').val(res.gender);
                $('#Bday').val(res.birthday);
               
            } else {
                console.error('Error saving data:', response.message);
                alert('Error saving data: ' + response.message);
            }
        },
        error: function(xhr, status, error) {
            const responses = JSON.parse(xhr.responseText);
        }
    });
}

//Видалення студента
function deleteStudentById(id) {

    $.ajax({
        url: `http://localhost/lab4/php/DeleteById.php?id=${id}`,
        method: 'DELETE',
        success: function(response) {

            if (response) {
                console.log('Data saved successfully:', response);

                getStudents();
               
            } else {
                console.error('Error saving data:', response.message);
                alert('Error saving data: ' + response.message);
            }
        },
        error: function(xhr, status, error) {
            const responses = JSON.parse(xhr.responseText);
        }
    });
}

//Редагування студента
function updateStudent(studentData){

    var jsonData = JSON.stringify(studentData);

 $.ajax({
        url: `http://localhost/lab4/php/updateStudent.php?id=${studentData.id}`,
        method: 'PUT',
        data: jsonData,
        success: function(response) {
            if (response) {
                console.log('Data saved successfully:', response);
              
                $('#Group').val('');
                $('#fname').val('');
                $('#lname').val('');
                $('#Gender').val('');
                $('#Bday').val('');

                closeForm();
                getStudents();

            } else {
                console.error('Error saving data:', response.message);
                alert('Error saving data: ' + response.message);
            }
        },
        error: function(xhr, status, error) {
            const responses = JSON.parse(xhr.responseText); 
            let message = ''; 
    
            responses.forEach(function(item) { 
                message += item.message + '\n';  
            }); 
         
            alert(message); 
           
        }
    });

    
}

function closeFormIn(){
    $('#login').val('');
    $('#password').val('');
    $('#form-sign-in').toggleClass('hidden'); 
    $('#overlay').hide();
}

function signIn(){

    var login = $('#login').val();
    var password = $('#password').val();
  
   /*  if(!validationIn(login, password)){
        return;
    } */

    var userData = {
        login: login,
        password: password
    }
    
    var jsonData = JSON.stringify(userData);

    $.ajax({ 
        url: 'http://localhost/lab4/php/LogIn.php', 
        type: 'POST', 
        contentType: 'application/json', 
        dataType: 'json', 
        data: jsonData, 
        success: function(data) { 
            console.log(data.success); 
            if (data.success) { 
                alert('User authenticated successfully'); 
                console.log('User authenticated successfully'); 
                closeFormIn();
            } else { 
                alert('Invalid username or password'); 
                console.log('Invalid username or password'); 
            } 
        }, 
        error: function(xhr, status, error) { 
            alert(JSON.parse(xhr.responseText).error);
            //const responses = JSON.parse(xhr.responseText); 
            console.error('AJAX request failed:', error); 
        } 
    }); 
}

function validationIn(login, password){
    isValid = true;

    if(!isValidLogin(login)) isValid = false;
    if(!isValidPassword(password)) isValid = false;

    return isValid;
}

function isValidLogin(login){

    if(!login){
        $('#login-error').css('display', 'block');
        $('#login-error').text('Обов\'язкове поле*');
        $('#login').css("border-color", 'red');
        return false;
    }
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const isEmail = emailRegex.test(login);
    if (!isEmail) {

        $('#login-error').css('display', 'block');
        $('#login-error').text('Логін повинен мати формат електронної адреси.');
        $('#login').css("border-color", 'red');
        return false;
    }
    else{
        $('#login-error').text('');
        $('#login').css("border-color", '');
        return true;
    }

}

function isValidPassword(password){
    if(!password){
        $('#password-error').css('display', 'block');
        $('#password-error').text('Обов\'язкове поле*');
        $('#password').css("border-color", 'red');
        return false;
    }
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
    const isPasswordValid = passwordRegex.test(password);
    if (!isPasswordValid) {
        $('#password-error').css('display', 'block');
        $('#password-error').text('Пароль повинен містити мінімум 8 символів, маленьку літеру, велику літеру та цифру.');
        $('#password').css("border-color", 'red');
        return false;
    }else{
        $('#password-error').text('');
        $('#password').css("border-color", '');
        return true;
    }

}
//Відкриття форми для входу
function toggleFormIN() {
    var form = document.getElementById('form-sign-in');
    form.classList.toggle('hidden');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block';
}