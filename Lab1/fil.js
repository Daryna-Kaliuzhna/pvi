function toggleForm() {
    var form = document.getElementById('student-form');
    form.classList.toggle('hidden');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block';
}

function addStudent() {
    
    var group = document.getElementById('Group').value;
    var firstname =  document.getElementById('fname').value;
    var lastname =  document.getElementById('lname').value;
    var gender =  document.getElementById('Gender').value;
    var birthday = document.getElementById('Bday').value;

    var table = document.querySelector(".student-table tbody");

       var newRow = document.createElement('tr');

    newRow.innerHTML = `
        <td><input type="checkbox"></td>
        <td>${group}</td>
        <td>${firstname} ${lastname}</td>
        <td>${gender}</td>
        <td>${birthday}</td>
        <td class="circle-cell">
            <div class="circle"></div>
        </td>
        <td>
            <button><img id="img-edit" src="./images/pen.png" alt="edit student"></button> 
            <button id="btn-delete" onclick="warningForm(this)"><img id="img-delete" src="./images/delete.png" alt="delete student"></button>
        </td>
    `;

    table.appendChild(newRow);

    document.getElementById('Group').value = '';
    document.getElementById('fname').value = '';
    document.getElementById('lname').value = '';
    document.getElementById('Gender').value = '';
    document.getElementById('Bday').value = '';
    cancelAdding()
}
function cancelAdding(){
    var form = document.getElementById('student-form');
    form.classList.toggle('hidden');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'none'; 

}
function cancelDeleting(){
    var form = document.getElementById('warning-form');
    form.classList.toggle('delete-form');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'none'; 
}

let currentRowToDelete;

function warningForm(button){
    var form = document.getElementById('warning-form');
    form.classList.toggle('delete-form');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block'; 

    currentRowToDelete = button.closest('tr');
   
    var studentNameElement = currentRowToDelete.querySelector('td:nth-child(3)');

    var studentName = studentNameElement.innerText;
  
    document.getElementById("warning-message").innerText = "Are you sure you want to delete user " + studentName + "?";
}

function deleteStudent() {
    currentRowToDelete.remove();
    cancelDeleting(); 
}