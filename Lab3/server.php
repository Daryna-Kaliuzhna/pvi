<?php  
$responses=[];
header("Access-Control-Allow-Origin: *");   
header("Access-Control-Allow-Methods: *");    
header("Access-Control-Allow-Headers: Content-Type"); 

function isValidData($data){

    $isValid = true;
    
    if(!isDataEntered($data)) $isValid = false;
    if(!isValidName($data)) $isValid = false;
    if(!isValidSurname($data)) $isValid = false;
    if(!isValidGender($data)) $isValid = false;
    if(!isValidGroup($data)) $isValid = false;
    if(!isValidNameFormat($data)) $isValid = false;
    if(!isValidDateFormat($data)) $isValid = false;

    return $isValid;
}
function isDataEntered($data){
    global $responses;
    if (!isset($data['firstname'], $data['lastname'], $data['gender'], $data['birthday'], $data['group'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Incomplete data received.'  
        ];  
       return false; 
    }  
    return true;
}

function isValidName($data){
    global $responses;  
    if (empty($data['firstname'])) {
        $responses[] = [  
            'success' => false,  
            'message' => 'FirstName error.' 
        ];  
        return false;
    }  
    return true;
}
function isValidSurname($data){
    global $responses;  
    if (empty($data['lastname'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Lastname error.'
        ];  
        return false;
    }  
    return true;
}
function isValidGender($data){
    global $responses;
    if (!isset($data['gender'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Gender error.'  
        ];  
        return false;
    }  
    return true;
}
function isValidGroup($data){
    global $responses;
    if (!isset($data['group'])) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Group error.'  
        ];  
        return false;
    }  
    return true;
}
function isValidNameFormat($data){
    global $responses;
    $firstname = $data['firstname'];  
    $lastname = $data['lastname'];  
    if (!preg_match('/^[A-Za-zА-Яа-яЁё]/u', $firstname) || !preg_match('/^[A-Za-zА-Яа-яЁё]/u', $lastname)) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'First name and last name must start with a Latin or Cyrillic letter.'  
        ];  
        return false;  
    }  
    return true;
}
function isValidDateFormat($data){
    global $responses;
    $birthday = $data['birthday'];  

    $minYear = 1990;  
    $maxYear = 2006;  
    if (strtotime($birthday) < strtotime("$minYear-01-01") || strtotime($birthday) > strtotime("$maxYear-12-31")) {  
        $responses[] = [  
            'success' => false,  
            'message' => 'Birthday must be between 1990 and 2006.'  
        ];  
        return false;  
    } 
    return true;
}
    
if ($_SERVER['REQUEST_METHOD'] === 'POST') {  
 
    $data = json_decode(file_get_contents('php://input'), true); 
 
    if(isValidData($data)){
        $responses[] = [  
            'success' => true,  
            'message' => 'Data received successfully.' 
        ]; 
        echo json_encode($responses, JSON_PRETTY_PRINT);
    }else{
        http_response_code(400);
        echo json_encode($responses, JSON_PRETTY_PRINT);
    }

} else {  
    $responses[] = [  
        'success' => false,  
        'message' => 'The query method is not supported.'  
    ];  
    http_response_code(405);  
}  
?>