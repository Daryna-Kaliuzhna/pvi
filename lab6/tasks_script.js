
document.addEventListener('DOMContentLoaded', function () {

    const urlParams = new URLSearchParams(window.location.search);
    const userSiteName = urlParams.get('user-site-name');
    const userSiteId = urlParams.get('user-id');
    const userSiteNameElement = document.querySelector('.user-site-name');
    const userSiteIdElement = document.querySelector('.user-site-id');

    if (userSiteName && userSiteId) {
        userSiteNameElement.innerText = userSiteName;
        userSiteIdElement.innerText = userSiteId;
    }

    const cancelTaskButtons = document.querySelectorAll('.btn-cancel-task');
    const addTaskForms = document.querySelector('.add-task-form');
    const formType = document.getElementById('form-type');
    const userId = document.querySelector('.user-site-id').innerText;

    clickAddTask();

    socket.emit('getTasks', userId);
    socket.on('tasks', tasks =>{
        tasks.forEach(task=>{
            const taskElement = createTaskElement(task.name, task.description, formatDate(task.date), task._id);
            appendTaskElement(taskElement, task.statusId);
        })
    })

    addTaskForms.addEventListener('submit', function (event) {
        event.preventDefault();

        const taskName = addTaskForms.querySelector('#taskName').value;
        const taskDescription = addTaskForms.querySelector('#taskDescription').value;
        const taskDeadline = addTaskForms.querySelector('#taskDeadline').value;
        const columnTypeInput = document.getElementsByName("column-type")[0];
        const columnName = columnTypeInput.value;
        const userId = document.querySelector('.user-site-id').innerText;

         const taskData = {
            userId: userId,
            name: taskName,
            description: taskDescription,
            date: taskDeadline,
            statusId: columnName
        } 
       
        if (formType.innerText == 'add') {
            socket.emit('addTask', taskData)
            socket.on('taskAdded', taskData =>{
                const taskElement = createTaskElement(taskData.name, taskData.description, taskDeadline, taskData._id);
                appendTaskElement(taskElement, taskData.statusId);
                resetForm();
            })

        }
        else if (formType.innerText == 'edit') {

            const taskId = addTaskForms.querySelector('input[name="taskId"]').value;
            const newColumn = addTaskForms.querySelector('#taskColumn');

            const updatedTaskData = {
                _id: taskId,
                userId: userId,
                name: taskName,
                description: taskDescription,
                date: taskDeadline,
                statusId: newColumn.value
            } 

            socket.emit('editTask', updatedTaskData)
            socket.on('taskUpdated', updatedTask =>{
                editTask(updatedTask);
                resetForm();
            })
        }
    });

    cancelTaskButtons.forEach((button) => {
        button.addEventListener('click', function () {
            resetForm();
        });
    });
});
function deleteTask(button)
{
    var deleteButtons = document.querySelectorAll('.btn-delete-task');


    deleteButtons.forEach(function(button) {
    button.addEventListener('click', function() {
   
        var body = this.closest('.card-body');
        var task = this.closest('.task');
       
        body.parentNode.removeChild(task);
    });
    });
}

function clickAddTask(){
    const addTaskButtons = document.querySelectorAll('.btn-add-task');
    const addTaskForms = document.querySelector('.add-task-form');
    const overlay = document.getElementById('overlay');
    const columnTypeInput = document.getElementsByName("column-type")[0];
    const formType = document.getElementById('form-type');

    addTaskButtons.forEach((button) => {
        button.addEventListener('click', function () {
            formType.innerText = 'add';
            const columnSelector = document.getElementById('select')
            columnSelector.setAttribute('hidden', 'true');
            addTaskForms.style.display = 'block';
            overlay.style.display = 'block';
            var columnName = button.closest('.card').querySelector('.card-header').textContent.trim();
            columnTypeInput.value = columnName;
        });
    });

}


function createTaskElement(name, description, data, _id) {
    const taskElement = document.createElement('div');
    taskElement.classList.add('task');
    taskElement.style.backgroundColor = 'rgba(200, 196, 196, 0.274)';
    taskElement.style.borderRadius = '5px';
    taskElement.innerHTML = `
        <div class="task-content">
            <input type="text" name="task-id" id="task-id" value="${_id}" hidden>
            <h5 id="name-task">${name}</h5>
            <p id="description">${description}</p>
            <p id="deadline">${data}</p>
        </div>
        <div class="task-actions d-flex justify-content-center align-items-center">
            <button class="btn btn-sm btn-primary btn-edit-task"  onclick = 'clickEditTask(this)'>Edit</button>
            
        </div>
    `;
    return taskElement;
}

function appendTaskElement(taskElement, statusId) {
    const Column = getTaskColumn(statusId);
    Column.appendChild(taskElement);
    overlay.style.display = 'none';
}

function getTaskColumn(statusId) {
    
    //const columnTypeInput = document.getElementsByName("column-type")[0];
   // const columnName = columnTypeInput.value;
    let Column;
    if (statusId === 'Todo') {
        Column = document.querySelector('.todo-body');
    } else if (statusId === 'In Progress') {
        Column = document.querySelector('.in-progress-body');
    } else if (statusId === 'Done') {
        Column = document.querySelector('.done-body');
    }
    return Column;
}

function resetForm() {
    const addTaskForms = document.querySelector('.add-task-form');
    addTaskForms.style.display = 'none';
    addTaskForms.querySelector('#taskName').value = '';
    addTaskForms.querySelector('#taskDescription').value = '';
    addTaskForms.querySelector('#taskDeadline').value = '';
    overlay.style.display = 'none';
}

let currentTaskContent;
let  currentTask;

function clickEditTask(button) {

    button.addEventListener('click', function () {
        currentTaskContent = button.closest('.task').querySelector('.task-content');
        currentTask = button.closest('.task');
        const addTaskForms = document.querySelector('.add-task-form');
        
        const dataDeadLine = document.getElementById('taskDeadline');
        const dataName = document.getElementById('taskName'); 
        const dataDescription = document.getElementById('taskDescription');
        const dataId = document.getElementById('taskId');
        
        
        dataName.value = currentTaskContent.querySelector('#name-task').textContent;
        dataDescription.value = currentTaskContent.querySelector('#description').innerText;
        dataDeadLine.value = currentTaskContent.querySelector('#deadline').innerText;
        dataId.value = currentTaskContent.querySelector('input[name="task-id"]').value;

        const formType = document.getElementById('form-type');

        const columnSelector = document.getElementById('select')
        formType.innerText = 'edit';
        columnSelector.removeAttribute('hidden');
    
        const overlay = document.getElementById('overlay');
        addTaskForms.style.display = 'block';
        overlay.style.display = 'block';
    });
}

function editTask(updatedTask){
    currentTask.innerHTML = `
    <div class="task-content">
    <input type="text" name="task-id" id="task-id" value="${updatedTask._id}" hidden>
        <h5 id="name-task">${updatedTask.name}</h5>
        <p id="description">${updatedTask.description}</p>
        <p id="deadline">${formatDate(updatedTask.date)}</p>
    </div>
    <div class="task-actions d-flex justify-content-center align-items-center">
        <button class="btn btn-sm btn-primary btn-edit-task"  onclick = 'clickEditTask(this)'>Edit</button>
    </div>
`;
   appendTaskElement(currentTask, updatedTask.statusId);

    /* const addTaskForms = document.querySelector('.add-task-form');
    const dataDeadLine = addTaskForms.querySelector('#taskDeadline').value;
    const dataDescription = addTaskForms.querySelector('#taskDescription').value;
    const dataName = addTaskForms.querySelector('#taskName').value;
   
    const previousColumn = addTaskForms.querySelector('input[name="column-type"]') 
    const taskId = addTaskForms.querySelector('input[name="task-id"]').value;
    
   previousColumn.value = newColumn.value;
    const addTaskForms = document.querySelector('.add-task-form');
     
    updatedTask.statusId = newColumn.value;*/
   
}

function formatDate(isoDateString) {
    const date = new Date(isoDateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
    const day = String(date.getDate()).padStart(2, '0');
    
    return `${year}-${month}-${day}`;
}