const mongoose = require('mongoose');
const mongodb = 'mongodb+srv://darynakaliuzhnapz2022:0kg8gRrjYY46vWDE@cluster.e0csq7l.mongodb.net/chat?retryWrites=true&w=majority'

mongoose.connect(mongodb).then(() => {
    console.log('connected')
}).catch(err => console.log(err))

const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

app.set('view', './views')
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

const userSchema = new mongoose.Schema({
    username: String,
    userId: Number,
    rooms: [{type: mongoose.Schema.Types.ObjectId, ref: 'Chat'}],
    lastActive: { type: Date, default: Date.now }
})
const user = mongoose.model('users', userSchema)

const counterSchema = new mongoose.Schema({
    name: String,
    seq: Number,
})
const counter = mongoose.model('counter', counterSchema)

const roomSchema = new mongoose.Schema({
    roomname: String,
    messages: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Message' }]
})
const Room = mongoose.model('rooms', roomSchema);

const messageSchema = new mongoose.Schema({
    message: String,
    username: String,
    userId: { type: Number, ref: 'User' },
    room: { type: mongoose.Schema.Types.ObjectId, ref: 'Chat' },
    createdAt: { type: Date, default: Date.now }
});


app.get('/', (req, res) => {
  
    res.json(Object.values(rooms))
})

app.get('/AccessibleRooms', async (req, res) => {
    const userID = req.query.userID; 

    const accessibleChats = await getRoomsForUser(userID);
    res.send(Object.values(accessibleChats));

    /* 
    const roomsWithUser = Object.entries(rooms).filter(([roomName, room]) => {
        return Object.keys(room.users).includes(userID);
    }).map(([roomName, room]) => {
        return room;
    });

    res.json(roomsWithUser); */
});

async function getRoomsForUser(user_id) {
    const user = await getUserById(user_id);
    let accessibleChats = [];

    if(user){
        const userChatsIds = user.rooms;
        
        for (let chatId of userChatsIds) {
            const chat = await Room.findOne({ _id : chatId });
    
            if(chat){
                accessibleChats.push(chat);
            }
        }
    }

    return accessibleChats;
}
async function getUserById(userId) {
    try {
        const us = await user.findOne({ userId: userId });

        if (!us) {
            console.log('User not found');
            return null;
        }

        console.log('User found:', us);
        return us;
    } catch (error) {
        console.error('Error when receiving a user:', error);
        throw error;
    }
}


app.get('/:roomId/:userName', async (req, res) => {
    
    try {
        const room = req.params.roomId;
        const userName = req.params.userName;
        const roomExists = await Room.exists({ _id: room });
        const roomData = await Room.findById(room);
        if (!roomExists) {
            res.json({
                url: `http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`,
                users: null
            });
        }
            const users = await user.find({ rooms: room });
            res.json({
                url: `http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}&roomName=${roomData.roomname}`,
                users: Object.values(users)
            });
    }catch (error) {
        console.error('Error when searching for users by room ID:', error);
        throw error;
    }
});

app.post('/login/:username/:userId', async (req, res) => {
    
    const username = req.params.username;
    const userId = req.params.userId;
    console.log('Received user id ', userId);

    try {
        const newUser = await addUserToDb(username, userId);
        res.json({
            userID: newUser.userId
        });
    } catch (error) {
        console.error('Error while processing a login request:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});

app.get('/getConnectedUsers', async (req, res) => {
   
    try {
        const users = await user.find();
        res.json(users);
    } catch (error) {
        console.error('Error when receiving users:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
})

/* app.post('/room', (req, res) => {
    //console.log(req.socket.id);
    const roomName = req.body.room;
    const userName = req.body.username
    if (rooms[roomName] != null) {
        return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`);
    } else {
        rooms[roomName] = { name: roomName, users: {} };
        io.emit('room-created', roomName);
        return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`)
    }
}) */

app.post('/room', async(req, res) => {
    
    const userIdsString = req.query.userIds; 
    const userIds = userIdsString.split(','); 

    const roomName = req.query.room;
    const userName = req.query.username
    
    await createChatWithUsers(roomName, userIds)
    return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`)
  
    //  if (rooms[roomName] != null) {
    //     return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`);
    // } else {
    //     rooms[roomName] = { name: roomName, users: selectedUsers };
    //     //io.emit('room-created', roomName);
    //     console.log(rooms);
    //    return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`)
    // } 
}) 

io.on('connection', socket => {

    socket.on('new-user', async (roomId, userId) => { 
        const us = await user.findOne({ userId: userId });
        const room = await Room.findById(roomId);
        const roomName = room.roomname;

        const messageModel = await getMessageModel(roomName,roomId, messageSchema);

        const messages = await messageModel.find();
        
        const result = messages.map(message => ({
            userId: message.userId,
            username: message.username,
            message: message.message
        }));

        socket.join(roomId)

        socket.emit('load-messages', result);
        console.log(us);
        
        socket.broadcast.to(roomId).emit('user-connected', us.username);
        updateLastActive(userId)
            .then(user => console.log('Updated user:', user))
            .catch(error => console.error('Error:', error));
    });

    socket.on('send-chat-message', async (room, roomId, message, userId) => {
        const us = await user.findOne({ userId: userId });
        const name = us.username;

        const messageModel = await getMessageModel(room,roomId, messageSchema);
        const mess = new messageModel({
            message: message,
            username: name,
            userId: userId,
            room: roomId
        })
        await mess.save();

        const chat = await Room.findById(roomId);
        chat.messages.push(mess);
        await chat.save();
        
        socket.broadcast.to(roomId).emit('chat-message', { message: message, name: name })
        updateLastActive(userId)
            .then(user => console.log('Updated user:', user))
            .catch(error => console.error('Error:', error));
    })

    socket.on('disconnect-from-room',async (roomId, userId)=> {
        try {
            const us = await user.findOne({ userId: userId });
            if (!us) {
                console.log('User not found');
                return;
            }
            socket.leave(roomId)
            socket.broadcast.to(roomId).emit('user-disconnected',  us.username)
            updateLastActive(userId)
                .then(user => console.log('Updated user:', user))
                .catch(error => console.error('Error:', error));
               
           
        } catch (error) {
            console.error('Error finding user or room:', error);
        }
    })

    socket.on('getNewMessagesCount', async (userId) => {
        try {
            console.log(userId);
            const result = await getNewMessagesCount(userId);
            socket.emit('newMessagesCount', result);
        } catch (error) {
            socket.emit('error', 'Error fetching new messages count');
        }
    });

    /*socket.on('disconnect', () => {
        const userId = socket.userId;
        console.log(userId)
        getUserRooms(userId).forEach(room => {
            console.log(room)
            socket.broadcast.to(room).emit('user-disconnected', rooms[room].users[userId]);
            delete rooms[room].users[userId];
        });
    }); */

    /* function getUserRooms(userId){ 
        return Object.entries(rooms).reduce((names, [name, room]) =>{ 
            if(room.users[userId] != null) names.push(name); 
            return names; 
        }, []) 
    } */

    socket.on('addTask', async (taskData) => {
        try {
            console.log(taskData);
            const newTask = new Task({
                userId: taskData.userId,
                name: taskData.name,
                description: taskData.description,
                date: taskData.date,
                statusId: taskData.statusId
            });

            console.log('New task:', newTask);
            await newTask.save();
            socket.emit('taskAdded', newTask);
        } catch (error) {
            console.error('Error adding task:', error);
        }
    });

    socket.on('getTasks', async (userId) => {
        try {
            const tasks = await Task.find({ userId: userId });
            socket.emit('tasks', tasks);
        } catch (error) {
            console.error('Error adding task:', error);
        }
    });

    socket.on('editTask', async (updatedTaskData) => {
        try {
            const taskId = updatedTaskData._id;
            await Task.findByIdAndUpdate(taskId, {
                name: updatedTaskData.name,
                description: updatedTaskData.description,
                date: updatedTaskData.date,
                statusId: updatedTaskData.statusId
            });
    
            const updatedTask = await Task.findById(taskId);
    
            socket.emit('taskUpdated', updatedTask);
        } catch (error) {
            console.error('Error editing task:', error);
        }
    });    

})

server.listen(3000) 


async function addUserToDb(name, userId) {
    try {
        const existingUser = await user.findOne({ userId : userId });

        if (existingUser) {
            console.log('A user with this userId already exists:', existingUser);
            return existingUser; 
        }

        const newUser = new user({
            username: name,
            userId: userId,
            chats: []
        });

        const savedUser = await newUser.save();
        console.log('User successfully added:', savedUser);
        return savedUser;
    } catch (error) {
        console.error('Error adding a user:', error);
        throw error;
    }
}
async function getNextUserId(){
  const count = await counter.findOneAndUpdate(
    {name: 'userId'},
    {$inc:{seq: 1}},
    {upsert:true, new: true}
  )
  return count.seq;
}

async function createChatWithUsers(name, userIds) {
    try {
        console.log(userIds)
        const chat = new Room({ roomname: name});
        await chat.save();
        for (let userId of userIds) {
            
            const us = await user.findOne({ userId: userId });
            if (us) {
                addChatToUser(chat._id, userId);
                console.log(userId + " added ")
            }
        }
        await chat.save();

        //const messageModel = await getMessageModel(name,chat._id, messageSchema)
      
        return chat._id;
    } catch (error) {
       
    }
}
async function addChatToUser(chatId, userId) {
    try {
      
        const us = await user.findOne({ userId: userId });

        if (!us) {
            console.error('User not found!');
            return;
        }

        if (us.rooms.includes(chatId)) {
            console.log('The chat has already been added to the users list of chats.');
            return;
        }

        us.rooms.push(chatId);

        await us.save();

        console.log('The chat has been successfully added to the users list of chats.');
    } catch (error) {
        console.error('Error adding a chat to the users chat list:', error);
    }
}

async function getMessageModel(chatName, chatId, messageSchema) { 
    const modelName = `${chatName.replace(/\s/g, '')}_${chatId}_messages`; 
 
    if (mongoose.models[modelName]) { 
        return mongoose.models[modelName]; 
    } 
    return mongoose.model(modelName, messageSchema); 
}


const updateLastActive = async (userId) => {
    try {
        const result = await user.findOneAndUpdate(
            { userId: userId },
            { lastActive: Date.now() },
            { new: true }
        );
        return result;
    } catch (error) {
        console.error('Error updating last active:', error);
        throw error;
    }
};

const getNewMessagesCount = async (userId) => {
    try {
        const userObj = await user.findOne({ userId: userId });
        if (!userObj) {
            throw new Error('User not found');
        }

        const userLastActive = userObj.lastActive;

        const result = [];

        for (const roomId of userObj.rooms) {
           
            const room = await Room.findById(roomId);

            const messageModel = await getMessageModel(room.roomname, room._id, messageSchema);
            const newMessagesCount = await messageModel.countDocuments({
                room: room._id,
                createdAt: { $gt: userLastActive }
            });

            if (newMessagesCount > 0) {
                result.push({
                    roomId: room._id,
                    roomName: room.roomname,
                    newMessagesCount: newMessagesCount
                });
            }
        }

        return result;
    } catch (error) {
        console.error('Error fetching new messages count:', error);
        throw error;
    }
};


//ТАСКИ

const taskSchema = new mongoose.Schema({ 
    userId: { 
        type: Number, 
        required: true 
    }, 
    name: { 
        type: String, 
        required: true 
    }, 
    description: { 
        type: String 
    }, 
    date: { 
        type: Date, 
        required: true 
    }, 
    statusId: { 
        type: String, 
        required: true 
    }, 
    createdAt: { 
        type: Date, 
        default: Date.now 
    } 
}); 

const Task = mongoose.model('Task', taskSchema); 
