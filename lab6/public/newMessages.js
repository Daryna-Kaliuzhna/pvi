let userID;

const getNewMessagesCount = (userId) => {
    socket.emit('getNewMessagesCount', userId);
    userID = userId;
};

socket.on('newMessagesCount', (data) => {
    console.dir(data);

    const ulElement = document.getElementById('notification-list');
    const userName = document.querySelector(".user-site-name").textContent;
    var userId = document.querySelector("#user-site-id").textContent;

    // Очищення списку перед додаванням нових елементів
    ulElement.innerHTML = '';

    // Перевірка наявності нових повідомлень
    if (data.length > 0) {
        document.querySelector('.bell-dropdown').classList.add('has-new-messages');
    } else {
        document.querySelector('.bell-dropdown').classList.remove('has-new-messages');
    }

    data.forEach(notification => {
        const liElement = document.createElement('li');
        liElement.onclick = () => sendJoinRoom(notification.roomId, userName);

        const imgElement = document.createElement('img');
        imgElement.src = 'images/user-24.png';
        imgElement.alt = 'user';

        const spanElement = document.createElement('span');
        const aElement = document.createElement('a');
        aElement.textContent = `${notification.roomName}: ${notification.newMessagesCount} new messages`;
        aElement.href = `./views/chat_index.ejs?user-site-name=${userName}&user-id=${userId}&room-id=${notification.roomId};`

        spanElement.appendChild(aElement);
        liElement.appendChild(imgElement);
        liElement.appendChild(spanElement);

        ulElement.appendChild(liElement);
    });
});