let globalStudentsId;

function toggleForm() {
    var form = document.getElementById('student-form');
    form.classList.toggle('hidden');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block';
}

$(document).ready(function() {
    $('#btn-add').on('click', function() {
        $('#form-header h2').text('Add Student');
        toggleForm();
    });
    
    $('#cancel-btn').on('click', function() {

        $('#Group, #fname, #lname, #Gender, #Bday').val('');
        $('#student-form').toggleClass('hidden'); 
        $('#overlay').hide();
    });

});


//Кнопка форми save
function save() {
    var header = $('#form-header h2').text();
    if (header === 'Add Student') {
        addStudent();
    } else {
        editStudent();
    }
}


//Зміна форми для редагування
function editOnClick(button) {
    
    currentRowToEdit = $(button).closest('tr');
    var id = $(currentRowToEdit).find('td:nth-child(1)').text();
    var group = $(currentRowToEdit).find('td:nth-child(3)').text();
    var fullname = $(currentRowToEdit).find('td:nth-child(4)').text();
    var gender = $(currentRowToEdit).find('td:nth-child(5)').text();
    var birthday = $(currentRowToEdit).find('td:nth-child(6)').text();
    var parts = fullname.split(' ');
    var firstname = parts[0];
    var lastname = parts[1];

    $('#studentId').val(id);
    $('#Group').val(group);
    $('#fname').val(firstname);
    $('#lname').val(lastname);
    $('#Gender').val(gender);
    $('#Bday').val(birthday);

    $('#form-header h2').text('Edit Student');
    toggleForm();
}

//Додавання студента
function addStudent() {

    var group = $('#Group').val();
    var firstname = $('#fname').val();
    var lastname = $('#lname').val();
    var gender = $('#Gender').val();
    var birthday = $('#Bday').val();

    if (!fieldsValidator(group, firstname, lastname, gender, birthday)) {
        return;
    }

    var id = ++globalStudentsId;
    $('#studentId').val(id);

    var newRow = `
        <tr>
            <td id="id-table"></td>
            <td><input type="checkbox"></td>
            <td>${group}</td>
            <td>${firstname} ${lastname}</td>
            <td>${gender}</td>
            <td>${birthday}</td>
            <td class="circle-cell">
                <div class="circle"></div>
            </td>
            <td>
                <button id="btn-edit" onclick="editOnClick(this)" class="btn-edit"><img id="img-edit" src="./images/pen.png" alt="edit student"></button> 
                <button id="btn-delete" onclick="warningForm(this)"><img id="img-delete" src="./images/delete.png" alt="delete student"></button>
            </td>
        </tr>
    `;

    $('.student-table tbody').append(newRow);
   

    var studentData = {
        id: id,
        group: group,
        firstname: firstname,
        lastname: lastname,
        gender: gender,
        birthday: birthday
    };

    var jsonData = JSON.stringify(studentData);

    $.ajax({
        url: 'http://127.0.0.1:5500/data.json',
        method: 'POST',
        contentType: 'application/json',
        data: jsonData,
        success: function(response) {
            console.log('Data saved successfully:', response);
        },
        error: function(xhr, status, error) {
            console.error('Error saving data:', error);
        }
    });
    
    closeForm();

}


//Закриття форми
function closeForm(){
    $('#Group, #fname, #lname, #Gender, #Bday').val('');
    $('#student-form').toggleClass('hidden'); 
    $('#overlay').hide();
}

//Редагування студента

let currentRowToEdit;

function editStudent(){
    var id = $('#studentId').val();
    var group = $('#Group').val();
    var firstname = $('#fname').val();
    var lastname = $('#lname').val();
    var gender = $('#Gender').val();
    var birthday = $('#Bday').val();

    if (!fieldsValidator(group, firstname, lastname, gender, birthday)) {
        return;
    }


    currentRowToEdit[0].innerHTML = `
        <td id="id-table"></td>
        <td><input type="checkbox"></td>
        <td>${group}</td>
        <td>${firstname} ${lastname}</td>
        <td>${gender}</td>
        <td>${birthday}</td>
        <td class="circle-cell">
            <div class="circle"></div>
        </td>
        <td>
            <button id="btn-edit" onclick="editOnClick(this)" class="btn-edit"><img id="img-edit" src="./images/pen.png" alt="edit student"></button> 
            <button id="btn-delete" onclick="warningForm(this)"><img id="img-delete" src="./images/delete.png" alt="delete student"></button>
        </td>
    `;

    closeForm();
 

    $.ajax({
        url: 'http://127.0.0.1:5500/data.json',
        method: 'GET',
        contentType: 'application/json',
        success: function(response) {
            console.log('Data saved successfully:', response);
        },
        error: function(xhr, status, error) {
            console.error('Error saving data:', error);
        }
    });
    
}

//Валідація даних
function fieldsValidator(group, firstname, lastname, gender, birthday){
    if (group === '' || firstname === '' || lastname === '' || gender === '' || birthday === '') {
        alert('Please fill in all fields');
        return false;
    }

    var nameRegex = /^[a-zA-Zа-яА-Я]+$/;
    if (!nameRegex.test(firstname) || !nameRegex.test(lastname)) {
        alert('First name and last name should contain only letters');
        return false;
    }

    var minDate = new Date('1990-01-01');
    var maxDate = new Date('2006-12-31');
    var inputDate = new Date(birthday);
    if (inputDate < minDate || inputDate > maxDate) {
        alert('Birthday should be between 1990 and 2006');
        return false;
    }

    return true;
}


//Видалення студента

let currentRowToDelete;

function warningForm(button){
    var form = document.getElementById('warning-form');
    form.classList.toggle('delete-form');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'block'; 

    currentRowToDelete = button.closest('tr');
   
    var studentNameElement = currentRowToDelete.querySelector('td:nth-child(4)');

    var studentName = studentNameElement.innerText;
  
    document.getElementById("warning-message").innerText = "Are you sure you want to delete user " + studentName + "?";
}
function deleteStudent() {
    globalStudentsId--;
    currentRowToDelete.remove();
    cancelDeleting(); 
}
function cancelDeleting(){
    var form = document.getElementById('warning-form');
    form.classList.toggle('delete-form');

    var overlay = document.getElementById('overlay');
    overlay.style.display = 'none'; 
}
