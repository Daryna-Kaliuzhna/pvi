
const CACHE_NAME = 'my-site-cache-v1'; 
 
const urlsToCache = [ 
  'index.html',
  'style.css',
  'animation.css',
  'fil.js',
  'images/delete.png',
  'images/bell-3-24.png',
  'images/close-x-10324.png',
  'images/pen.png',
  'images/plus.png',
  'images/shlyapa.png',
  'images/user-24.png'
]; 
 

self.addEventListener('install', function(event) { 
  event.waitUntil( 
    caches.open(CACHE_NAME) 
      .then(function(cache) { 
        console.log('Opened cache'); 
        return cache.addAll(urlsToCache); 
      }) 
  ); 
}); 
 
self.addEventListener('fetch', function(event) { 
  event.respondWith( 
    caches.match(event.request) 
      .then(function(response) { 
        if (response) { 
          return response; 
        } 
        return fetch(event.request); 
      }) 
  ); 
});