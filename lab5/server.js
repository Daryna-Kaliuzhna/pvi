
const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

app.set('view', './views')
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

const rooms = {}
const connectedUsers = {}

let nextUserID = 1;

app.get('/', (req, res) => {
  
    res.json(Object.values(rooms))
})

app.get('/AccessibleRooms', (req, res) => {
    const userID = req.query.userID; 
   
    const roomsWithUser = Object.entries(rooms).filter(([roomName, room]) => {
        return Object.keys(room.users).includes(userID);
    }).map(([roomName, room]) => {
        return room;
    });

    res.json(roomsWithUser);
});

app.get('/:room/:userName', (req, res) => {
    const roomName = req.params.room;
    const userName = req.params.userName;

    if (rooms[roomName] != null) {
        const users = rooms[roomName].users;
        res.json({
            url: `http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}&roomName=${roomName}`,
            users: Object.values(users)
        });
    } else {
        res.json({
            url: `http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`,
            users: null
        });
    }
});

app.post('/login/:username', (req, res) => {
    
    const username = req.params.username;
    const existingUser = Object.values(connectedUsers).find(user => user === username);
  
    if (existingUser) {

        const id = Object.keys(connectedUsers).find(key => connectedUsers[key] === existingUser);
        return res.json({
            userID: id
        });
    }

    const userID = nextUserID++;
    connectedUsers[userID] = username;

    res.json({
        userID: userID
    });
});

app.get('/getConnectedUsers', (req, res) => {
   
    res.json(connectedUsers)
})

/* app.post('/room', (req, res) => {
    //console.log(req.socket.id);
    const roomName = req.body.room;
    const userName = req.body.username
    if (rooms[roomName] != null) {
        return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`);
    } else {
        rooms[roomName] = { name: roomName, users: {} };
        io.emit('room-created', roomName);
        return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`)
    }
}) */

app.post('/room', (req, res) => {
   console.log(req);
    console.log(rooms);

    const userIdsString = req.query.userIds; 
    const userIds = userIdsString.split(','); 

    const roomName = req.query.room;
    const userName = req.query.username
    
    const selectedUsers = {};
    
    Object.entries(connectedUsers).forEach(([userId, username]) => {
        if (userIds.includes(userId)) {
            selectedUsers[userId] = username;
            console.log("added " + username);
        }
    });

    if (rooms[roomName] != null) {
        return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`);
    } else {
        rooms[roomName] = { name: roomName, users: selectedUsers };
        //io.emit('room-created', roomName);
        console.log(rooms);
       return res.redirect(`http://localhost/lab4/views/chat_index.ejs?user-site-name=${userName}`)
    }
}) 

io.on('connection', socket => {

    socket.on('new-user', (room, userId) => {
        socket.userId = userId
        socket.join(room)
        console.log(room)
        const name = connectedUsers[userId]
        console.log(name + " " + userId);
        console.log(rooms[room].users[userId] + " " + userId);
        if(rooms[room].users[userId] == name){
            console.log(rooms[room])
            socket.broadcast.to(room).emit('user-connected', name)
        }
    })

    socket.on('send-chat-message', (room, message, userId) => {
        const name = connectedUsers[userId]
        if(rooms[room].users[userId] == name){
            socket.broadcast.to(room).emit('chat-message', { message: message, name: name })
        }
    })

    socket.on('disconnect-from-room', (room, userId) => {
        const name = connectedUsers[userId]
        if(rooms[room].users[userId] == name){
            console.log(rooms[room])
            socket.leave(room)
            socket.broadcast.to(room).emit('user-disconnected', name)
         // delete rooms[room].users[socket.id]
        }
    })

    /* socket.on('disconnect', () => {
        const userId = socket.userId;
        console.log(userId)
        getUserRooms(userId).forEach(room => {
            console.log(room)
            socket.broadcast.to(room).emit('user-disconnected', rooms[room].users[userId]);
            delete rooms[room].users[userId];
        });
    }); */

    function getUserRooms(userId){ 
        return Object.entries(rooms).reduce((names, [name, room]) =>{ 
            if(room.users[userId] != null) names.push(name); 
            return names; 
        }, []) 
    }
})

server.listen(3000)