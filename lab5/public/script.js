
const socket = io('http://localhost:3000', { transports: ['websocket'] })
const messageForm = document.getElementById('chat-form')
const messageInput = document.getElementById('message-input')



socket.on('chat-message', data => {
    appendMessage(`${data.name}: ${data.message}`)
})

socket.on('user-connected', name => {
    appendMessage(`${name} connected`, "center")
})

socket.on('user-disconnected', name => {
    appendMessage(`${name} disconnected`, "center")

    const userListElement = document.querySelector('#room-users');
    const userElements = userListElement.querySelectorAll('li');

    userElements.forEach(userElement => {

        if (userElement.innerText === name) {
            userElement.remove();
        }
    });
})

document.addEventListener('DOMContentLoaded', async function () {
    const urlParams = new URLSearchParams(window.location.search);
    const userSiteName = urlParams.get('user-site-name');
    const userSiteNameElement = document.querySelector('.user-site-name');
    const usernameInput = document.querySelector('input[name="username"]');

    if (userSiteName) {
        usernameInput.value = userSiteName;
        userSiteNameElement.innerText = userSiteName;
        console.log(usernameInput.value)

        await fetch(`http://localhost:3000/login/${usernameInput.value}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username: usernameInput.value })
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to log in');
            }
            return response.json();
        })
        .then(data => {
            
            console.log('User ID:', data.userID);
            document.getElementById('userId').value = data.userID;

        })
        .catch(error => {
            console.error('Authentication error:', error.message);
        });
    }
    getRoomsList();
});

function getConnectedUsers() {
    fetch('http://localhost:3000/getConnectedUsers')
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('Connected Users:', data);
            $("#connectedUsersDropdown").empty();

            const userDropdown = document.getElementById('connectedUsersDropdown');

            var allKeys = Object.keys(data);
            const userId = document.getElementById('userId').value;

            allKeys.forEach(function (key) {
                console.log(key)
                if (userId != key) {
                    var label = document.createElement("label");

                    var checkbox = document.createElement("input");
                    checkbox.type = "checkbox";
                    checkbox.value = key;
                    checkbox.classList.add("item-drop");
                    label.appendChild(checkbox);

                    label.appendChild(document.createTextNode(data[key]));
                    label.className = "dropdown-item";

                    $("#connectedUsersDropdown").append(label);
                }
            });
            $("#connectedUsersDropdown").css("display", "block");
        })
        .catch(error => {
            console.error('Error getting connected users:', error.message);
        });
}

messageForm.addEventListener('submit', e => {
    e.preventDefault()
    const roomName = document.querySelector('#room-title').innerText;
    const userId = document.getElementById('userId').value;
    const message = messageInput.value
    appendMessage(`${message}`, "right")
    socket.emit('send-chat-message', roomName, message, userId)
    messageInput.value = ''
})

function appendMessage(message, alignment = 'left') {
    const chatMessagesElement = document.querySelector('.chat-messages');

    const messageElement = document.createElement('div');
    const messageWrap = document.createElement('div');
    messageWrap.classList.add('message-wrap');
    messageWrap.classList.add('mt-2');
    messageWrap.style.display = 'flex';

    messageElement.classList.add('message');
    messageElement.classList.add('d-inline-block');
    messageElement.classList.add('text-white');

    messageElement.style.backgroundColor = 'rgba(80, 156, 237, 0.680)';
    messageElement.style.borderRadius = '10px';
    messageElement.style.padding = '10px';


    if (alignment === 'right') {
        messageWrap.classList.add('justify-content-end');
    } else if (alignment === 'center') {

        messageElement.style.backgroundColor = 'rgba(31, 119, 138, 0.344)';
        messageElement.style.padding = '5px';
        messageWrap.classList.add('justify-content-center');
    }

    const messageContent = document.createElement('p');

    messageContent.style.margin = '0';
    messageContent.innerText = message;

    messageElement.appendChild(messageContent);
    messageWrap.appendChild(messageElement);

    chatMessagesElement.appendChild(messageWrap);
}

socket.on('room-created', room => {
    const urlParams = new URLSearchParams(window.location.search);
    const userSiteName = urlParams.get('user-site-name');
    const roomListItem = document.createElement('li');
    roomListItem.classList.add('list-group-item', 'chat-item');

    const roomLink = document.createElement('a');
    roomLink.href = `http://localhost/lab4/views/chat_index.ejs?user-site-name=${userSiteName}&roomName=${room}`;
    roomLink.classList.add('room-link');
    roomLink.dataset.chatName = `Chat with ${room}`;
    roomLink.dataset.username = room;

    const img = document.createElement('img');
    img.src = 'https://via.placeholder.com/50';
    img.style.width = '50px';
    img.style.height = '50px';
    img.classList.add('img-fluid', 'rounded-circle', 'mr-2');
    img.alt = room;

    const span = document.createElement('span');
    span.textContent = room;

    roomLink.appendChild(img);
    roomLink.appendChild(span);

    roomListItem.appendChild(roomLink);

    const roomList = document.querySelector('.list-group');
    roomList.appendChild(roomListItem);
});

function getRoomsList() {
    const urlParams = new URLSearchParams(window.location.search);
    const userSiteName = urlParams.get('user-site-name');
    const roomName = urlParams.get('roomName');
    const userId = document.getElementById('userId').value;
    fetch(`http://localhost:3000/AccessibleRooms?userID=${userId}`, {
        method: 'GET'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(function (rooms) {
        rooms.forEach(room => {
            const isActive = room.name == roomName ? "active" : "";
            const li = `
        <li class="list-group-item chat-item ${isActive}" onclick = "joinRoom(this, event)" >
            <a href="" class="room-link" data-chat-name="${room.name}" data-username="${room.name}">
                <img src="https://via.placeholder.com/50" style="width: 50px; height: 50px;" class="img-fluid rounded-circle mr-2" alt="${room.name}">
                ${room.name}
            </a>
        </li>`;
            document.querySelector('.list-group.mt-3').innerHTML += li;
        });
    })
    .catch(error => console.error('Error getting rooms: ', error));
}

function joinRoom(li, event) {
    const chatMessagesDiv = document.querySelector('.chat-messages');
    const userId = document.getElementById('userId').value;
    chatMessagesDiv.innerHTML = '';
    event.preventDefault();
    appendMessage('You joined', "center")

    const usernameInput = document.querySelector('input[name="username"]');

    const chatForm = document.getElementById('chat-form');
    chatForm.classList.remove('hidden');

    if (usernameInput) {
        const oldRoomName = document.querySelector('#room-title').innerText
        const username = usernameInput.value;
        const roomLinks = document.querySelectorAll('.room-link');

        if (oldRoomName.trim() !== '') {

            socket.emit('disconnect-from-room', oldRoomName, userId);
        }

        roomLinks.forEach(roomLink => {
            roomLink.closest('li').classList.remove('active');
        });


        const roomName = li.querySelector('.room-link').dataset.chatName;


        li.classList.add('active');

        fetch(`http://localhost:3000/${roomName}/${username}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {

                const url = new URL(data.url);
                const users = data.users
                const urlParams = new URLSearchParams(url.search);
                const roomName = urlParams.get('roomName');

                document.querySelector('#room-title').innerText = roomName;
                const userListTitle = document.querySelector('#users-title');
                userListTitle.innerHTML = "Users:";

                const userListElement = document.querySelector('#room-users');
                userListElement.innerHTML = "";
                users.forEach(user => {
                    const userElement = document.createElement('li');
                    userElement.innerText = user;
                    userElement.classList.add("mr-2");
                    userListElement.appendChild(userElement);
                });

            })
            .catch(error => console.error('Error:', error));
        socket.emit('new-user', roomName, userId );
    }
}

function CreateRoomWithUsers() {
    const urlParams = new URLSearchParams(window.location.search);
    const userSiteName = urlParams.get('user-site-name');
    const roomName = document.querySelector('input[name="room"]').value;
    const userId = document.getElementById('userId').value;

    var checkboxes = document.querySelectorAll('.item-drop');
    var selectedValues = [];
   
    checkboxes.forEach(function (checkbox) {
        
        if (checkbox.checked) {
            
            selectedValues.push(checkbox.value);
        }
    });
   
    selectedValues.push(userId);
    const userIdsString = selectedValues.join(',');
  
    const formData = {
        room: roomName, 
        username: userSiteName 
    };

    fetch(`http://localhost:3000/room?userIds=${userIdsString}&room=${roomName}&username=${userSiteName}`, {
        method: 'POST', 
        headers: {
            'Content-Type': 'application/json' 
        }
    })
    .then(response => {
        
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json(); 
    })
    .then(data => {
        
        console.log('Success:', data);
    })
    .catch(error => {
        
        console.error('Error:', error);
    }); 
}