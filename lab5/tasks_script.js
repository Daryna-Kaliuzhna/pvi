document.addEventListener('DOMContentLoaded', function () {

    const urlParams = new URLSearchParams(window.location.search);
    const userSiteName = urlParams.get('user-site-name');
    const userSiteNameElement = document.querySelector('.user-site-name');

    if (userSiteName) {
        userSiteNameElement.innerText = userSiteName;
        //alert( userSiteNameElement.innerText)
    }


    
    const cancelTaskButtons = document.querySelectorAll('.btn-cancel-task');
    const addTaskForms = document.querySelector('.add-task-form');
    const formType = document.getElementById('form-type');
    clickAddTask();

    addTaskForms.addEventListener('submit', function (event) {
        event.preventDefault();

        const taskName = addTaskForms.querySelector('#taskName').value;
        const taskDescription = addTaskForms.querySelector('#taskDescription').value;
        const taskDeadline = addTaskForms.querySelector('#taskDeadline').value;

        if (formType.innerText == 'add') {

            const taskElement = createTaskElement(taskName, taskDescription, taskDeadline);
            appendTaskElement(taskElement);
            resetForm();

        }
        else if (formType.innerText == 'edit') {
            editTask();
            resetForm();
        }


    });

    cancelTaskButtons.forEach((button) => {
        button.addEventListener('click', function () {
            resetForm();
        });
    });

    


});
function deleteTask(button)
{
    var deleteButtons = document.querySelectorAll('.btn-delete-task');


    deleteButtons.forEach(function(button) {
    button.addEventListener('click', function() {
   
        var body = this.closest('.card-body');
        var task = this.closest('.task');
       
        body.parentNode.removeChild(task);
    });
    });
}

function clickAddTask(){
    const addTaskButtons = document.querySelectorAll('.btn-add-task');
    const addTaskForms = document.querySelector('.add-task-form');
    const overlay = document.getElementById('overlay');
    const columnTypeInput = document.getElementsByName("column-type")[0];
    const formType = document.getElementById('form-type');

    addTaskButtons.forEach((button) => {
        button.addEventListener('click', function () {
            formType.innerText = 'add';
            //alert(formType.innerText);
            const columnSelector = document.getElementById('select')
            columnSelector.setAttribute('hidden', 'true');
            addTaskForms.style.display = 'block';
            overlay.style.display = 'block';
            var columnName = button.closest('.card').querySelector('.card-header').textContent.trim();
            columnTypeInput.value = columnName;
        });
    });

}


function createTaskElement(name, description, deadline) {
    const taskElement = document.createElement('div');
    taskElement.classList.add('task');
    taskElement.style.backgroundColor = 'rgba(200, 196, 196, 0.274)';
    taskElement.style.borderRadius = '5px';
    taskElement.innerHTML = `
        <div class="task-content">
            <h5 id="name-task">${name}</h5>
            <p id="description">${description}</p>
            <p id="deadline">${deadline}</p>
        </div>
        <div class="task-actions d-flex justify-content-center align-items-center">
            <button class="btn btn-sm btn-primary btn-edit-task"  onclick = 'clickEditTask(this)'>Edit</button>
            
        </div>
    `;
    return taskElement;
}
//<button class="btn btn-danger btn-sm btn-delete-task" onclick = 'deleteTask(this)'>Delete</button>

function appendTaskElement(taskElement) {
    const Column = getTaskColumn();
    Column.appendChild(taskElement);
    overlay.style.display = 'none';
}

function getTaskColumn() {
    
    const columnTypeInput = document.getElementsByName("column-type")[0];
    const columnName = columnTypeInput.value;
    let Column;
    if (columnName === 'Todo') {
        Column = document.querySelector('.todo-body');
    } else if (columnName === 'In Progress') {
        Column = document.querySelector('.in-progress-body');
    } else if (columnName === 'Done') {
        Column = document.querySelector('.done-body');
    }
    return Column;
}

function resetForm() {
    const addTaskForms = document.querySelector('.add-task-form');
    addTaskForms.style.display = 'none';
    addTaskForms.querySelector('#taskName').value = '';
    addTaskForms.querySelector('#taskDescription').value = '';
    addTaskForms.querySelector('#taskDeadline').value = '';
    overlay.style.display = 'none';
}

let currentTaskContent;
let  currentTask;

function clickEditTask(button) {

        button.addEventListener('click', function () {
            currentTaskContent = button.closest('.task').querySelector('.task-content');
            currentTask = button.closest('.task');
            const addTaskForms = document.querySelector('.add-task-form');
            
            const dataDeadLine = document.getElementById('taskDeadline');
            const dataDescription = document.getElementById('taskDescription');
            const dataName = document.getElementById('taskName');
        
            dataName.value = currentTaskContent.querySelector('#name-task').textContent;
            dataDescription.value = currentTaskContent.querySelector('#description').innerText;
            dataDeadLine.value = currentTaskContent.querySelector('#deadline').innerText;

            const formType = document.getElementById('form-type');

            const columnSelector = document.getElementById('select')
            formType.innerText = 'edit';
            columnSelector.removeAttribute('hidden');
        
            const overlay = document.getElementById('overlay');
            addTaskForms.style.display = 'block';
            overlay.style.display = 'block';
        });
}

function editTask(){
    const addTaskForms = document.querySelector('.add-task-form');
    const dataDeadLine = addTaskForms.querySelector('#taskDeadline').value;
    const dataDescription = addTaskForms.querySelector('#taskDescription').value;
    const dataName = addTaskForms.querySelector('#taskName').value;
    const newColumn = addTaskForms.querySelector('#taskColumn');
    const previousColumn = addTaskForms.querySelector('input[name="column-type"]') 

    previousColumn.value = newColumn.value;
   
    currentTask.innerHTML = `
    <div class="task-content">
        <h5 id="name-task">${dataName}</h5>
        <p id="description">${dataDescription}</p>
        <p id="deadline">${dataDeadLine}</p>
    </div>
    <div class="task-actions d-flex justify-content-center align-items-center">
        <button class="btn btn-sm btn-primary btn-edit-task"  onclick = 'clickEditTask(this)'>Edit</button>
    </div>
`;

   appendTaskElement(currentTask);
}
